const PENDING = 'pending';
const FULFILLED = 'fulfilled';
const REJECTED = 'rejected';

class OwnPromise {
  #storageCb = [];
  #state = PENDING;
  #value;
  #resolve;
  #reject;

  constructor(executor) {
    if (!executor.call || this.#value !== undefined || this.#state !== PENDING || typeof this !== 'object') {
      throw new TypeError();
    }

    const settle = value => {
      if (this.constructor !== OwnPromise) {
        throw new TypeError();
      }

      this.#value = value;

      setTimeout(() => {
        this.#storageCb.forEach(item => {
          if (this.#state === FULFILLED) {
            item.onFulfilledHeandler(this.#value);
            return;
          }

          item.onRejectedHeandler(this.#value);
        });
      }, 0);
    };

    this.#resolve = res => {
      if (this.#state !== PENDING) {
        return;
      }

      settle(res);
      this.#state = FULFILLED;
    };

    this.#reject = err => {
      if (this.#state !== PENDING) {
        return;
      }

      settle(err);
      this.#state = REJECTED;
    };

    try {
      executor(this.#resolve, this.#reject);
    }
    catch (err) {
      this.#reject(err);
    }
  }

  static resolve(value) {
    if (this !== OwnPromise) {
      throw new TypeError();
    }

    if (value instanceof OwnPromise) {
      return value;
    }

    return new OwnPromise((res, rej) => res(value));
  }

  static reject(reason) {
    if (this !== OwnPromise) {
      throw new TypeError();
    }

    return new OwnPromise((res, rej) => rej(reason));
  }

  static race(iterable) {
    if (this !== OwnPromise) {
      throw new TypeError();
    }

    return new OwnPromise((res, rej) => {
      iterable.forEach(item => item.then(res, rej));
    });
  }

  static all(iterable) {
    if (this !== OwnPromise) {
      throw new TypeError();
    }

    if (typeof iterable[Symbol.iterator] !== 'function') {
      return new OwnPromise((res, rej) => rej(new TypeError()));
    }

    if (iterable.length === 0) {
      return new OwnPromise(res => res(new Array(0)));
    }

    return new OwnPromise((res, rej) => {
      const arr = [];

      iterable.forEach(item => {
        item
          .then(value => arr.push(value), error => rej(error))
          .then(() => res(arr));
      });
    });
  }

  then(onFulfilled, onRejected) {
    if (this.constructor !== OwnPromise) {
      throw new TypeError();
    }

    if (typeof onFulfilled !== 'function' && !onRejected) {
      return this;
    }

    return new OwnPromise((res, rej) => {
      let newValue = null;

      const onFulfilledHeandler = value => {
        if (typeof onFulfilled === 'function') {
          try {
            newValue = onFulfilled(value);
          } catch (err) {
            rej(err);
          }
        }

        res(newValue);
      };

      const onRejectedHeandler = error => {
        if (typeof onRejected === 'function') {
          try {
            newValue = onRejected(error);
          } catch (err) {
            rej(err);
          }

          res(newValue);
        }

        rej(error);
      };

      if (this.#state === PENDING) {
        this.#storageCb.push({ onFulfilledHeandler, onRejectedHeandler });
        return;
      }

      setTimeout(() => {
        if (typeof onFulfilledHeandler !== 'function' || typeof onRejectedHeandler !== 'function') {
          throw this.#value;
        }

        if (this.#state === FULFILLED) {
          onFulfilledHeandler(this.#value);
          return;
        }

        onRejectedHeandler(this.#value);
      }, 0);
    });
  }

  catch(onRejected) {
    return this.then(null, onRejected);
  }
}

module.exports = OwnPromise;
